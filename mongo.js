//Query Operators
	//allow for more flexible querying in MongoDB 
	//Instead of just being able to find/search for documents with exact values
	//we can use to query operators define conditions instead of just specific criterias

//$gt, $lt, $gte and $lte - numbers

	//$gt - greater than
	db.products.find({"price" : {$gt : 3000}})

	//$gte - greater than or equal to
	db.products.find({"price" : {$gte : 30000}})

	//$lt - less than

	//$lte - less than or equal to

//note: query operators can be use to expand the search criteria for updating and deleting

	db.products.updateMany(
		{"price" : {$gte : 30000}}, 
		{$set : {"isActive" : false}}
	)

db.users.insertMany([
	{
		"firstName" : "Mary Jane",
		"lastName" : "Watson",
		"email" : "mjtiger@gmail.com",
		"password" : "tigerjackpot15",
		"isAdmin" : false
	},

	{
		"firstName" : "Gwen",
		"lastName" : "Stacy",
		"email" : "stacyTech@gmail.com",
		"password" : "stacyTech1991",
		"isAdmin" : true
	},

	{
		"firstName" : "Peter",
		"lastName" : "Parker",
		"email" : "peterWebDev@gmail.com",
		"password" : "webDeveloperPeter",
		"isAdmin" : true
	},

	{
		"firstName" : "Jonah",
		"lastName" : "Jameson",
		"email" : "jjjameson@gmail.com",
		"password" : "spideyisamenace",
		"isAdmin" : false
	},

	{
		"firstName" : "Otto",
		"lastName" : "Octavius",
		"email" : "ottoOctopi@gmail.com",
		"password" : "docOck15",
		"isAdmin" : true
	}
])

//$regex - this query operator will allow us to find documents in which it will match the characters/pattern of the character we are looking for

	db.users.find({
		"firstName" : {$regex : 'O'}
	})

	db.users.find({
		"lastName" : {$regex : 'W'}
	})

	//$options

	db.users.find({
		"lastName" : {$regex : 'o', $options: '$i'}
	})

//find documents that matches a specific word
	db.users.find({
		"email" : {$regex: 'web', $options : '$i'}
	})

	db.products.find({"name" : {$regex : 'phone', $options : '$i'}})

//mini-activity

db.products.find({"name" : {$regex : 'razer', $options : '$i'}})

db.products.find({"name" : {$regex : 'rakk', $options : '$i'}})

//$or and $and

	// $or operator - allows us to have a logical operation wherein we can look or find for a document which can satisfy at least one of our condition

	db.products.find({
		$or : [
			{"name" : {$regex : 'x', $options : '$i'}},
			{"price" : {$lte : 10000}}
		]
	})

	db.products.find({
		$or : [
			{"name" : {$regex : 'x', $options : '$i'}},
			{"price" : 30000}
		]
	})

	db.users.find({
		$or : [
			{"firstName" : {$regex : 'a', $options : '$i'}},
			{"isAdmin" : true}
		]
	})


//mini-activity
db.users.find({
	$or : [
		{"lastName" : {$regex : 'w', $options : '$i'}},
		{"isAdmin" : false}
	]
})


	//$and operator - allows us to have a logical operation wherein we can look or find for documents which can satisfy both the condition
	db.products.find({
		$and : [
			{"name" : {$regex : 'razer', $options : 'i'}},
			{"price" : {$gte : 3000}}
		]
	})

	db.users.find({
		$and : [
			{"lastName" : {$regex : 'w', $options : 'i'}},
			{"isAdmin" : false}
		]
	})

//mini-activity

db.users.find({
	$and : [
		{"lastName" : {$regex : 'a', $options : 'i'}},
		{"firstName" : {$regex : 'e', $options : 'i'}}
	]
})


//Field Projection
	//allows us to hide/show properties/field of the returned document foter a query
	//in field projection, 0means hide and 1 means show
	//db.collection.find({query}, {"field" : 0/1})

db.users.find({}, {"_id" : 0, "password" : 0})

//find() can have two arguments
	//db.collection.find({query}, {projection})

db.users.find({"isAdmin" : false}, {"_id" : 0, "email" : 1})

db.products.find({"price" : {$gte : 10000}}, {"_id" : 0, "name" : 1, "price" : 1})